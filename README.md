# Facturas

## Enunciado

1. En este proyecto tienes una página con el HTML y el CSS ya montados. El objetivo final del ejercicio será conseguir, aplicando Bootstrap, que la página se vea como las capturas que tienes en la carpeta diseños/. Sigue los siguientes pasos:
2. Copia el starter de Gulp + Bootstrap a este proyecto.
3. Instala las dependencias npm.
4. Lanza gulp para que abra la web en el navegador y se quede vigilando cambios.
5. Comprueba en el navegador que se está cargando Bootstrap.
6. Para conseguir el diseño sólo tienes que añadir clases al HTML. No tienes que modificar la estructura de etiquetas HTML ni tocar los archivos de SASS.
7. Oculta primero el formulario añadiéndole una clase de Bootstrap, para retomarlo más adelante.
8. Añade clases al HTML para que se vea como en los diseños. Ten en cuenta que:
    - el breakpoint para cambiar de versión móvil a versión desktop es 992px
    - la cabecera debe ser sticky
    - los elementos deben quedar alineados tal y como se ve en los PNG
    - fíjate en los espacios verticales: algunos elementos están separados entre sí con márgenes verticales, y éstos son diferentes en la versión móvil y en la versión desktop
    - en el formulario, el desplegable "Tipo:" aparece en primer lugar en la versión móvil y en último en la versión desktop
9. Cuando la página se vea bien, desoculta el formulario y añádele las clases correspondientes para que se vea como en las capturas.
10. Cuando el formulario se vea bien, añade al HTML lo que creas necesario para que el formulario se abra/cierre al pulsar el icono + de la cabecera.